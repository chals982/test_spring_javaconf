package com.websystique.springmvc.controller;

import java.util.HashMap;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



@Controller
@RequestMapping("/")
public class AppController {

	@RequestMapping(value = { "/"}, method = RequestMethod.GET)
	public String homePage(ModelMap model) {
		return "home";
	}

	@RequestMapping(value = { "/products"}, method = RequestMethod.GET)
	public String productsPage(ModelMap model) {
		return "products";
	}

	@RequestMapping(value = { "/contactus"}, method = RequestMethod.GET)
	public String contactUsPage(ModelMap model) {
		return "contactus";
	}
	

	@RequestMapping(value = { "/test"}, method = RequestMethod.GET)
	public String test(ModelMap model) {
		
		HashMap<String, String> testList = new HashMap<>();
		testList.put("aa", "홍길동");
		testList.put("bb", "어우동");
		testList.put("cc", "춘향이");
		testList.put("dd", "변사또");
		model.addAttribute("testList", testList);
		
		System.out.println("testList ::: " + testList.size());
		return "test";
	}
}