# README #

Spring java configuration 테스트

-----------------------

### 연동 내용 ###

* JDK 1.8
* Spring 4.3.7.RELEASE
* Tiles 3.0.7
* Freemarker 2.3.26-incubating

-----------------------

## 설명 ##

### 소스 링크 ###
* 원본 URL (류대리가 알려준 곳): http://websystique.com/springmvc/spring-4-mvc-apache-tiles-3-annotation-based-example/
* git URL (public 프로젝트): https://bitbucket.org/chals982/test_spring_javaconf/downloads/ 에서 "Download repository"
* 로컬 테스트 주소 : http://localhost:8080/test

### tiles 가 기본적으로 jsp를 지원하고 freemarker 와 velocit로 지원 한다는 내용 있음
* https://tiles.apache.org/framework/config-reference.html

### freemarker 지원에 대한 설명 세부로 들어 가면 xml 방식의 설정법이 나옴 ###
* https://tiles.apache.org/framework/tutorial/integration/freemarker.html

### xml 설정을 java config로 변경 하는 방법 ###
* http://www.concretepage.com/spring-4/spring-4-mvc-freemarker-template-annotation-integration-example-with-freemarkerconfigurer

### freemarker 문법 ###
* http://freemarker.org/docs/ref_directive_list.html
* 문법이 바뀐듯 함

### 기타 ###
* 센텀 CMS 기존의 freemarker 설정에도 보면 web.xml에 설정한 것 과 context.xml에 설정하는 2가지가 들어 감
* 위 freemarker 지원에 대한 설명에도 보면 두군데 설정을 넣게 되어 있음.
* 근데 테스트 하여 보면 context쪽에만 우선 설정하고 web.xml의 servlet 등록은 하지 않아도 잘 나옴 (Initializer.java)
* 이 이유는 좀 더 찾아 볼 것!  (ㅋㅋㅋ 찾음 나에게도 알려줘) 지금은 연동만 처리 함
* 로그를 보면 velocity도 올라옴 (별도로 사용하지 않게 하는 옵션이 있지 않을까 싶음 - 이것도 여기서는 제외 ㅎㅎ)


