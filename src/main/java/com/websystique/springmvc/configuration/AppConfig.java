package com.websystique.springmvc.configuration;

import org.apache.tiles.TilesContainer;
import org.apache.tiles.evaluator.AttributeEvaluatorFactory;
import org.apache.tiles.extras.complete.CompleteAutoloadTilesContainerFactory;
import org.apache.tiles.extras.complete.CompleteAutoloadTilesInitializer;
import org.apache.tiles.factory.AbstractTilesContainerFactory;
import org.apache.tiles.request.ApplicationContext;
import org.apache.tiles.request.freemarker.render.FreemarkerRendererBuilder;
import org.apache.tiles.request.render.BasicRendererFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;


@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.websystique.springmvc")
public class AppConfig extends WebMvcConfigurerAdapter{

	@Bean
	public TilesViewResolver tilesViewResolver() {
		TilesViewResolver resolver = new TilesViewResolver();
		resolver.setOrder(1);
		return resolver;
	}
	
	@Bean
	public TilesConfigurer tilesConfigurer(){
	    TilesConfigurer tilesConfigurer = new TilesConfigurer();
	    tilesConfigurer.setDefinitions(new String[] {"/WEB-INF/views/**/tiles.xml"});
	    tilesConfigurer.setCheckRefresh(true);

	    tilesConfigurer.setTilesInitializer(new CompleteAutoloadTilesInitializer() {
	    	@Override
	    	protected AbstractTilesContainerFactory createContainerFactory(ApplicationContext context) {
		    	return new CompleteAutoloadTilesContainerFactory() {
			    	@Override
			    	protected void registerAttributeRenderers(BasicRendererFactory rendererFactory, ApplicationContext applicationContext, TilesContainer container, AttributeEvaluatorFactory attributeEvaluatorFactory) {
				    	super.registerAttributeRenderers(rendererFactory, applicationContext, container, attributeEvaluatorFactory);
				    	//FreemarkerRenderer freemarkerRenderer = new FreemarkerRenderer(new AttributeValueFreemarkerServlet());
				    	FreemarkerRendererBuilder freemarkerRenderer = FreemarkerRendererBuilder.createInstance();
				    	freemarkerRenderer.setApplicationContext(applicationContext);
				    	//freemarkerRenderer.setParameter("TemplatePath", "/");
				    	//freemarkerRenderer.setParameter("NoCache", "true");
				    	freemarkerRenderer.setParameter("ContentType", "text/html;Charset=UTF-8");
				    	freemarkerRenderer.setParameter("default_encoding", "utf8");
				    	//freemarkerRenderer.setParameter("template_update_delay", "0");
				    	//freemarkerRenderer.setParameter("number_format", "0.##########");
				    	rendererFactory.registerRenderer("freemarker", freemarkerRenderer.build());
			    	}
		    	};
	    	}
	    });
	    

	    return tilesConfigurer;
	}

	@Bean
	public FreeMarkerViewResolver freemarkerViewResolver() {
		FreeMarkerViewResolver resolver = new FreeMarkerViewResolver();
		resolver.setCache(true);
		resolver.setPrefix("");
		resolver.setSuffix(".ftl");
		resolver.setOrder(2);
		resolver.setExposeSpringMacroHelpers(true);
		return resolver;
	}

	@Bean
	public FreeMarkerConfigurer freemarkerConfig() {
		FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
		freeMarkerConfigurer.setTemplateLoaderPath("/WEB-INF/views/");
		freeMarkerConfigurer.setDefaultEncoding("utf-8");
		return freeMarkerConfigurer;
	}
	
	/**
     * Configure ViewResolvers to deliver preferred views.
     */
	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		//TilesViewResolver viewResolver = new TilesViewResolver();
		//registry.viewResolver(viewResolver);
		
		registry.freeMarker();
		registry.tiles();
	}
	
	/**
     * Configure ResourceHandlers to serve static resources like CSS/ Javascript etc...
     */
	
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
    }
    
}

